# RxBus2
[![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![](https://jitpack.io/v/HossainCo/RxBus2.svg)](https://jitpack.io/#HossainCo/RxBus2)
[![Build Status](https://travis-ci.org/HossainCo/RxBus2.svg?branch=master)](https://travis-ci.org/HossainCo/RxBus2)
[![Coverage Status](https://coveralls.io/repos/github/HossainCo/RxBus2/badge.svg?branch=master)](https://coveralls.io/github/HossainCo/RxBus2?branch=master)
 
[![Github All Releases](https://img.shields.io/github/downloads/HossainCo/RxBus2/total.svg?style=flat-square)]()

This is a simple Rx2 Event Bus implementation using Kotlin. for RxJava2, RxKotlin2 and RxAndroid2 project

## How to use
### Registered
```kotlin
//subscribe to events (registered)
RxBus.observe<ExampleEvent>()
  .subscribe { doSomething() }
  .registerInBus(this) //registers your subscription to unsubscribe it properly later
 
//send events
RxBus.send(WhatEverEvent(someData))
 
// at the end
//unsubscribe from events
RxBus.unregister(this)
```
### UnRegistered
```kotlin
//subscribe to events (unregistered)
val disposable = RxBus.observe<ExampleEvent>()
  .subscribe { doSomething() }
  
// at the end for each disposable
disposable.dispose()
```    
### Bus Instance
```kotlin
val bus = RxBus // default instance
val bus = RxBus() // new Instance
```    
### Java usage
```java
final RxBus bus = RxBus.getInstance() // default instance
final RxBus bus = new RxBus() // new bus
```    

## Gradle
```Groovy
// in root project
allprojects {
  repositories {
    // ...
    maven { url 'https://jitpack.io' }
  }
}
 
// in project
dependencies {
  compile 'com.github.hossainco:rxbus2:0.1.3'
  // ...
}
```

## License
```text
MIT License
 
Copyright (c) 2017 HossainCo
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
